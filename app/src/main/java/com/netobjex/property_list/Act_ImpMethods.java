package com.netobjex.property_list;

import android.view.View;

public interface Act_ImpMethods {

	void initVariable();

	void initializeView(View v);

	void postInitView();

	void addAdapter();

	void loadData();


}
