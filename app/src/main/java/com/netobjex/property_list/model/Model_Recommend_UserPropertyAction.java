package com.netobjex.property_list.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Model_Recommend_UserPropertyAction {

    @SerializedName("liked")
    @Expose
    private Boolean liked;
    @SerializedName("visited")
    @Expose
    private Boolean visited;
    @SerializedName("userPropertyKey")
    @Expose
    private Model_Recommend_UserPropertyAction_UserPropertyKey userPropertyKey;

    public Boolean getLiked() {
        return liked;
    }

    public void setLiked(Boolean liked) {
        this.liked = liked;
    }

    public Boolean getVisited() {
        return visited;
    }

    public void setVisited(Boolean visited) {
        this.visited = visited;
    }

    public Model_Recommend_UserPropertyAction_UserPropertyKey getUserPropertyKey() {
        return userPropertyKey;
    }

    public void setUserPropertyKey(Model_Recommend_UserPropertyAction_UserPropertyKey userPropertyKey) {
        this.userPropertyKey = userPropertyKey;
    }
}
