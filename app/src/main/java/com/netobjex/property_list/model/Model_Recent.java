package com.netobjex.property_list.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Model_Recent {


    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("tnx")
    @Expose
    private String tnx;
    @SerializedName("property_id")
    @Expose
    private String propertyId;
    @SerializedName("property_type")
    @Expose
    private String propertyType;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("description_note")
    @Expose
    private String descriptionNote;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("ac_lot")
    @Expose
    private Double acLot;
    @SerializedName("building_area")
    @Expose
    private String buildingArea;
    @SerializedName("cap_rate")
    @Expose
    private Double capRate;
    @SerializedName("unit_price")
    @Expose
    private String unitPrice;
    @SerializedName("unit_building_size")
    @Expose
    private String unitBuildingSize;
    @SerializedName("unit_ac_lot")
    @Expose
    private String unitAcLot;
    @SerializedName("unit_cap_rate")
    @Expose
    private String unitCapRate;
    @SerializedName("address_city")
    @Expose
    private String addressCity;
    @SerializedName("address_postal_code")
    @Expose
    private String addressPostalCode;
    @SerializedName("address_state")
    @Expose
    private String addressState;
    @SerializedName("address_street")
    @Expose
    private String addressStreet;
    @SerializedName("address_country")
    @Expose
    private String addressCountry;
    @SerializedName("geo_code")
    @Expose
    private String geoCode;
    @SerializedName("number_of_rooms")
    @Expose
    private Double numberOfRooms;
    @SerializedName("bed_room")
    @Expose
    private Double bedRoom;
    @SerializedName("bath_room")
    @Expose
    private Double bathRoom;
    @SerializedName("car_space")
    @Expose
    private Double carSpace;
    @SerializedName("building_size")
    @Expose
    private Double buildingSize;
    @SerializedName("user_property_actions")
    @Expose
    private List<Model_Recent_UserPropertyAction> userPropertyActions = null;
    @SerializedName("user_view")
    @Expose
    private Model_Recent_UserView userView;
    @SerializedName("business_type")
    @Expose
    private Object businessType;
    @SerializedName("number_of_tenants")
    @Expose
    private Double numberOfTenants;
    @SerializedName("number_of_units")
    @Expose
    private Double numberOfUnits;
    @SerializedName("number_of_1_bedrooms")
    @Expose
    private Object numberOf1Bedrooms;
    @SerializedName("number_of_2_bedrooms")
    @Expose
    private Object numberOf2Bedrooms;
    @SerializedName("number_of_3_bedrooms")
    @Expose
    private Object numberOf3Bedrooms;
    @SerializedName("first_trust_deed_balance")
    @Expose
    private Object firstTrustDeedBalance;
    @SerializedName("second_trust_deed_balance")
    @Expose
    private Object secondTrustDeedBalance;
    @SerializedName("asking_price")
    @Expose
    private Object askingPrice;
    @SerializedName("seller_true_contact")
    @Expose
    private Object sellerTrueContact;
    @SerializedName("parcel_number_1")
    @Expose
    private Object parcelNumber1;
    @SerializedName("building_class")
    @Expose
    private String buildingClass;
    @SerializedName("building_condition")
    @Expose
    private Object buildingCondition;
    @SerializedName("pro_forma_cap_rate")
    @Expose
    private Object proFormaCapRate;
    @SerializedName("listing_broker_agent_first_name")
    @Expose
    private Object listingBrokerAgentFirstName;
    @SerializedName("listing_broker_agent_last_name")
    @Expose
    private Object listingBrokerAgentLastName;
    @SerializedName("land_sf_net")
    @Expose
    private Double landSfNet;
    @SerializedName("land_sf_net_unit")
    @Expose
    private String landSfNetUnit;
    @SerializedName("year_built")
    @Expose
    private Double yearBuilt;
    @SerializedName("frontage")
    @Expose
    private Object frontage;
    @SerializedName("studio_mix")
    @Expose
    private Object studioMix;
    @SerializedName("listing_broker_company")
    @Expose
    private Object listingBrokerCompany;
    @SerializedName("seller_true_address")
    @Expose
    private Object sellerTrueAddress;
    @SerializedName("land_area_sf")
    @Expose
    private Double landAreaSf;
    @SerializedName("land_area_sf_unit")
    @Expose
    private String landAreaSfUnit;
    @SerializedName("buyer_true_contact")
    @Expose
    private Object buyerTrueContact;
    @SerializedName("second_trust_deed_lender")
    @Expose
    private Object secondTrustDeedLender;
    @SerializedName("vacancy")
    @Expose
    private Object vacancy;
    @SerializedName("gim")
    @Expose
    private Object gim;
    @SerializedName("gross_income")
    @Expose
    private Double grossIncome;
    @SerializedName("gross_income_unit")
    @Expose
    private String grossIncomeUnit;
    @SerializedName("bldg_sf")
    @Expose
    private Object bldgSf;
    @SerializedName("sale_date")
    @Expose
    private Object saleDate;
    @SerializedName("two_bedroom_mix")
    @Expose
    private Object twoBedroomMix;
    @SerializedName("tenancy")
    @Expose
    private Object tenancy;
    @SerializedName("sale_status")
    @Expose
    private Object saleStatus;
    @SerializedName("grm")
    @Expose
    private Object grm;
    @SerializedName("number_of_studios")
    @Expose
    private Double numberOfStudios;
    @SerializedName("recording_date")
    @Expose
    private Object recordingDate;
    @SerializedName("buyer_true_company")
    @Expose
    private Object buyerTrueCompany;
    @SerializedName("building_materials")
    @Expose
    private String buildingMaterials;
    @SerializedName("down_payment")
    @Expose
    private Double downPayment;
    @SerializedName("down_payment_unit")
    @Expose
    private String downPaymentUnit;
    @SerializedName("one_bedroom_mix")
    @Expose
    private Object oneBedroomMix;
    @SerializedName("price_per_unit")
    @Expose
    private Double pricePerUnit;
    @SerializedName("price_per_unit_unit")
    @Expose
    private String pricePerUnitUnit;
    @SerializedName("roof_type")
    @Expose
    private Object roofType;
    @SerializedName("price_per_sf_land")
    @Expose
    private Double pricePerSfLand;
    @SerializedName("price_per_sf_land_unit")
    @Expose
    private String pricePerSfLandUnit;
    @SerializedName("age")
    @Expose
    private Object age;
    @SerializedName("first_trust_deed_payment")
    @Expose
    private Object firstTrustDeedPayment;
    @SerializedName("first_trust_deed_lender")
    @Expose
    private Object firstTrustDeedLender;
    @SerializedName("zoning")
    @Expose
    private Object zoning;
    @SerializedName("land_area_ac")
    @Expose
    private Double landAreaAc;
    @SerializedName("land_area_ac_unit")
    @Expose
    private String landAreaAcUnit;
    @SerializedName("parking_ratio")
    @Expose
    private Object parkingRatio;
    @SerializedName("three_bedroom_mix")
    @Expose
    private Object threeBedroomMix;
    @SerializedName("seller_true_company")
    @Expose
    private Object sellerTrueCompany;
    @SerializedName("units_per_acre")
    @Expose
    private Object unitsPerAcre;
    @SerializedName("floor_area_ratio")
    @Expose
    private Object floorAreaRatio;
    @SerializedName("price_per_sf_net")
    @Expose
    private Double pricePerSfNet;
    @SerializedName("price_per_sf_net_unit")
    @Expose
    private String pricePerSfNetUnit;
    @SerializedName("star_rating")
    @Expose
    private Double starRating;
    @SerializedName("user_rating")
    @Expose
    private Double userRating;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getTnx() {
        return tnx;
    }

    public void setTnx(String tnx) {
        this.tnx = tnx;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescriptionNote() {
        return descriptionNote;
    }

    public void setDescriptionNote(String descriptionNote) {
        this.descriptionNote = descriptionNote;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Double getAcLot() {
        return acLot;
    }

    public void setAcLot(Double acLot) {
        this.acLot = acLot;
    }

    public String getBuildingArea() {
        return buildingArea;
    }

    public void setBuildingArea(String buildingArea) {
        this.buildingArea = buildingArea;
    }

    public Double getCapRate() {
        return capRate;
    }

    public void setCapRate(Double capRate) {
        this.capRate = capRate;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getUnitBuildingSize() {
        return unitBuildingSize;
    }

    public void setUnitBuildingSize(String unitBuildingSize) {
        this.unitBuildingSize = unitBuildingSize;
    }

    public String getUnitAcLot() {
        return unitAcLot;
    }

    public void setUnitAcLot(String unitAcLot) {
        this.unitAcLot = unitAcLot;
    }

    public String getUnitCapRate() {
        return unitCapRate;
    }

    public void setUnitCapRate(String unitCapRate) {
        this.unitCapRate = unitCapRate;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressPostalCode() {
        return addressPostalCode;
    }

    public void setAddressPostalCode(String addressPostalCode) {
        this.addressPostalCode = addressPostalCode;
    }

    public String getAddressState() {
        return addressState;
    }

    public void setAddressState(String addressState) {
        this.addressState = addressState;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public String getAddressCountry() {
        return addressCountry;
    }

    public void setAddressCountry(String addressCountry) {
        this.addressCountry = addressCountry;
    }

    public String getGeoCode() {
        return geoCode;
    }

    public void setGeoCode(String geoCode) {
        this.geoCode = geoCode;
    }

    public Double getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(Double numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public Double getBedRoom() {
        return bedRoom;
    }

    public void setBedRoom(Double bedRoom) {
        this.bedRoom = bedRoom;
    }

    public Double getBathRoom() {
        return bathRoom;
    }

    public void setBathRoom(Double bathRoom) {
        this.bathRoom = bathRoom;
    }

    public Double getCarSpace() {
        return carSpace;
    }

    public void setCarSpace(Double carSpace) {
        this.carSpace = carSpace;
    }

    public Double getBuildingSize() {
        return buildingSize;
    }

    public void setBuildingSize(Double buildingSize) {
        this.buildingSize = buildingSize;
    }

    public List<Model_Recent_UserPropertyAction> getUserPropertyActions() {
        return userPropertyActions;
    }

    public void setUserPropertyActions(List<Model_Recent_UserPropertyAction> userPropertyActions) {
        this.userPropertyActions = userPropertyActions;
    }

    public Model_Recent_UserView getUserView() {
        return userView;
    }

    public void setUserView(Model_Recent_UserView userView) {
        this.userView = userView;
    }

    public Object getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Object businessType) {
        this.businessType = businessType;
    }

    public Double getNumberOfTenants() {
        return numberOfTenants;
    }

    public void setNumberOfTenants(Double numberOfTenants) {
        this.numberOfTenants = numberOfTenants;
    }

    public Double getNumberOfUnits() {
        return numberOfUnits;
    }

    public void setNumberOfUnits(Double numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }

    public Object getNumberOf1Bedrooms() {
        return numberOf1Bedrooms;
    }

    public void setNumberOf1Bedrooms(Object numberOf1Bedrooms) {
        this.numberOf1Bedrooms = numberOf1Bedrooms;
    }

    public Object getNumberOf2Bedrooms() {
        return numberOf2Bedrooms;
    }

    public void setNumberOf2Bedrooms(Object numberOf2Bedrooms) {
        this.numberOf2Bedrooms = numberOf2Bedrooms;
    }

    public Object getNumberOf3Bedrooms() {
        return numberOf3Bedrooms;
    }

    public void setNumberOf3Bedrooms(Object numberOf3Bedrooms) {
        this.numberOf3Bedrooms = numberOf3Bedrooms;
    }

    public Object getFirstTrustDeedBalance() {
        return firstTrustDeedBalance;
    }

    public void setFirstTrustDeedBalance(Object firstTrustDeedBalance) {
        this.firstTrustDeedBalance = firstTrustDeedBalance;
    }

    public Object getSecondTrustDeedBalance() {
        return secondTrustDeedBalance;
    }

    public void setSecondTrustDeedBalance(Object secondTrustDeedBalance) {
        this.secondTrustDeedBalance = secondTrustDeedBalance;
    }

    public Object getAskingPrice() {
        return askingPrice;
    }

    public void setAskingPrice(Object askingPrice) {
        this.askingPrice = askingPrice;
    }

    public Object getSellerTrueContact() {
        return sellerTrueContact;
    }

    public void setSellerTrueContact(Object sellerTrueContact) {
        this.sellerTrueContact = sellerTrueContact;
    }

    public Object getParcelNumber1() {
        return parcelNumber1;
    }

    public void setParcelNumber1(Object parcelNumber1) {
        this.parcelNumber1 = parcelNumber1;
    }

    public String getBuildingClass() {
        return buildingClass;
    }

    public void setBuildingClass(String buildingClass) {
        this.buildingClass = buildingClass;
    }

    public Object getBuildingCondition() {
        return buildingCondition;
    }

    public void setBuildingCondition(Object buildingCondition) {
        this.buildingCondition = buildingCondition;
    }

    public Object getProFormaCapRate() {
        return proFormaCapRate;
    }

    public void setProFormaCapRate(Object proFormaCapRate) {
        this.proFormaCapRate = proFormaCapRate;
    }

    public Object getListingBrokerAgentFirstName() {
        return listingBrokerAgentFirstName;
    }

    public void setListingBrokerAgentFirstName(Object listingBrokerAgentFirstName) {
        this.listingBrokerAgentFirstName = listingBrokerAgentFirstName;
    }

    public Object getListingBrokerAgentLastName() {
        return listingBrokerAgentLastName;
    }

    public void setListingBrokerAgentLastName(Object listingBrokerAgentLastName) {
        this.listingBrokerAgentLastName = listingBrokerAgentLastName;
    }

    public Double getLandSfNet() {
        return landSfNet;
    }

    public void setLandSfNet(Double landSfNet) {
        this.landSfNet = landSfNet;
    }

    public String getLandSfNetUnit() {
        return landSfNetUnit;
    }

    public void setLandSfNetUnit(String landSfNetUnit) {
        this.landSfNetUnit = landSfNetUnit;
    }

    public Double getYearBuilt() {
        return yearBuilt;
    }

    public void setYearBuilt(Double yearBuilt) {
        this.yearBuilt = yearBuilt;
    }

    public Object getFrontage() {
        return frontage;
    }

    public void setFrontage(Object frontage) {
        this.frontage = frontage;
    }

    public Object getStudioMix() {
        return studioMix;
    }

    public void setStudioMix(Object studioMix) {
        this.studioMix = studioMix;
    }

    public Object getListingBrokerCompany() {
        return listingBrokerCompany;
    }

    public void setListingBrokerCompany(Object listingBrokerCompany) {
        this.listingBrokerCompany = listingBrokerCompany;
    }

    public Object getSellerTrueAddress() {
        return sellerTrueAddress;
    }

    public void setSellerTrueAddress(Object sellerTrueAddress) {
        this.sellerTrueAddress = sellerTrueAddress;
    }

    public Double getLandAreaSf() {
        return landAreaSf;
    }

    public void setLandAreaSf(Double landAreaSf) {
        this.landAreaSf = landAreaSf;
    }

    public String getLandAreaSfUnit() {
        return landAreaSfUnit;
    }

    public void setLandAreaSfUnit(String landAreaSfUnit) {
        this.landAreaSfUnit = landAreaSfUnit;
    }

    public Object getBuyerTrueContact() {
        return buyerTrueContact;
    }

    public void setBuyerTrueContact(Object buyerTrueContact) {
        this.buyerTrueContact = buyerTrueContact;
    }

    public Object getSecondTrustDeedLender() {
        return secondTrustDeedLender;
    }

    public void setSecondTrustDeedLender(Object secondTrustDeedLender) {
        this.secondTrustDeedLender = secondTrustDeedLender;
    }

    public Object getVacancy() {
        return vacancy;
    }

    public void setVacancy(Object vacancy) {
        this.vacancy = vacancy;
    }

    public Object getGim() {
        return gim;
    }

    public void setGim(Object gim) {
        this.gim = gim;
    }

    public Double getGrossIncome() {
        return grossIncome;
    }

    public void setGrossIncome(Double grossIncome) {
        this.grossIncome = grossIncome;
    }

    public String getGrossIncomeUnit() {
        return grossIncomeUnit;
    }

    public void setGrossIncomeUnit(String grossIncomeUnit) {
        this.grossIncomeUnit = grossIncomeUnit;
    }

    public Object getBldgSf() {
        return bldgSf;
    }

    public void setBldgSf(Object bldgSf) {
        this.bldgSf = bldgSf;
    }

    public Object getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Object saleDate) {
        this.saleDate = saleDate;
    }

    public Object getTwoBedroomMix() {
        return twoBedroomMix;
    }

    public void setTwoBedroomMix(Object twoBedroomMix) {
        this.twoBedroomMix = twoBedroomMix;
    }

    public Object getTenancy() {
        return tenancy;
    }

    public void setTenancy(Object tenancy) {
        this.tenancy = tenancy;
    }

    public Object getSaleStatus() {
        return saleStatus;
    }

    public void setSaleStatus(Object saleStatus) {
        this.saleStatus = saleStatus;
    }

    public Object getGrm() {
        return grm;
    }

    public void setGrm(Object grm) {
        this.grm = grm;
    }

    public Double getNumberOfStudios() {
        return numberOfStudios;
    }

    public void setNumberOfStudios(Double numberOfStudios) {
        this.numberOfStudios = numberOfStudios;
    }

    public Object getRecordingDate() {
        return recordingDate;
    }

    public void setRecordingDate(Object recordingDate) {
        this.recordingDate = recordingDate;
    }

    public Object getBuyerTrueCompany() {
        return buyerTrueCompany;
    }

    public void setBuyerTrueCompany(Object buyerTrueCompany) {
        this.buyerTrueCompany = buyerTrueCompany;
    }

    public String getBuildingMaterials() {
        return buildingMaterials;
    }

    public void setBuildingMaterials(String buildingMaterials) {
        this.buildingMaterials = buildingMaterials;
    }

    public Double getDownPayment() {
        return downPayment;
    }

    public void setDownPayment(Double downPayment) {
        this.downPayment = downPayment;
    }

    public String getDownPaymentUnit() {
        return downPaymentUnit;
    }

    public void setDownPaymentUnit(String downPaymentUnit) {
        this.downPaymentUnit = downPaymentUnit;
    }

    public Object getOneBedroomMix() {
        return oneBedroomMix;
    }

    public void setOneBedroomMix(Object oneBedroomMix) {
        this.oneBedroomMix = oneBedroomMix;
    }

    public Double getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(Double pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public String getPricePerUnitUnit() {
        return pricePerUnitUnit;
    }

    public void setPricePerUnitUnit(String pricePerUnitUnit) {
        this.pricePerUnitUnit = pricePerUnitUnit;
    }

    public Object getRoofType() {
        return roofType;
    }

    public void setRoofType(Object roofType) {
        this.roofType = roofType;
    }

    public Double getPricePerSfLand() {
        return pricePerSfLand;
    }

    public void setPricePerSfLand(Double pricePerSfLand) {
        this.pricePerSfLand = pricePerSfLand;
    }

    public String getPricePerSfLandUnit() {
        return pricePerSfLandUnit;
    }

    public void setPricePerSfLandUnit(String pricePerSfLandUnit) {
        this.pricePerSfLandUnit = pricePerSfLandUnit;
    }

    public Object getAge() {
        return age;
    }

    public void setAge(Object age) {
        this.age = age;
    }

    public Object getFirstTrustDeedPayment() {
        return firstTrustDeedPayment;
    }

    public void setFirstTrustDeedPayment(Object firstTrustDeedPayment) {
        this.firstTrustDeedPayment = firstTrustDeedPayment;
    }

    public Object getFirstTrustDeedLender() {
        return firstTrustDeedLender;
    }

    public void setFirstTrustDeedLender(Object firstTrustDeedLender) {
        this.firstTrustDeedLender = firstTrustDeedLender;
    }

    public Object getZoning() {
        return zoning;
    }

    public void setZoning(Object zoning) {
        this.zoning = zoning;
    }

    public Double getLandAreaAc() {
        return landAreaAc;
    }

    public void setLandAreaAc(Double landAreaAc) {
        this.landAreaAc = landAreaAc;
    }

    public String getLandAreaAcUnit() {
        return landAreaAcUnit;
    }

    public void setLandAreaAcUnit(String landAreaAcUnit) {
        this.landAreaAcUnit = landAreaAcUnit;
    }

    public Object getParkingRatio() {
        return parkingRatio;
    }

    public void setParkingRatio(Object parkingRatio) {
        this.parkingRatio = parkingRatio;
    }

    public Object getThreeBedroomMix() {
        return threeBedroomMix;
    }

    public void setThreeBedroomMix(Object threeBedroomMix) {
        this.threeBedroomMix = threeBedroomMix;
    }

    public Object getSellerTrueCompany() {
        return sellerTrueCompany;
    }

    public void setSellerTrueCompany(Object sellerTrueCompany) {
        this.sellerTrueCompany = sellerTrueCompany;
    }

    public Object getUnitsPerAcre() {
        return unitsPerAcre;
    }

    public void setUnitsPerAcre(Object unitsPerAcre) {
        this.unitsPerAcre = unitsPerAcre;
    }

    public Object getFloorAreaRatio() {
        return floorAreaRatio;
    }

    public void setFloorAreaRatio(Object floorAreaRatio) {
        this.floorAreaRatio = floorAreaRatio;
    }

    public Double getPricePerSfNet() {
        return pricePerSfNet;
    }

    public void setPricePerSfNet(Double pricePerSfNet) {
        this.pricePerSfNet = pricePerSfNet;
    }

    public String getPricePerSfNetUnit() {
        return pricePerSfNetUnit;
    }

    public void setPricePerSfNetUnit(String pricePerSfNetUnit) {
        this.pricePerSfNetUnit = pricePerSfNetUnit;
    }

    public Double getStarRating() {
        return starRating;
    }

    public void setStarRating(Double starRating) {
        this.starRating = starRating;
    }

    public Double getUserRating() {
        return userRating;
    }

    public void setUserRating(Double userRating) {
        this.userRating = userRating;
    }

}
