package com.netobjex.property_list.model;

import com.google.gson.annotations.SerializedName;

public class Model_ProductList {

    public Model_ProductList() {

    }

    @SerializedName("campaigns_end_date")
    private String campaigns_end_date;
    @SerializedName("campaigns_image")
    private String campaigns_image;
    @SerializedName("campaigns_id")
    private String campaigns_id;

    public String getCampaigns_end_date() {
        return campaigns_end_date;
    }

    public void setCampaigns_end_date(String campaigns_end_date) {
        this.campaigns_end_date = campaigns_end_date;
    }

    public String getCampaigns_image() {
        return campaigns_image;
    }

    public void setCampaigns_image(String campaigns_image) {
        this.campaigns_image = campaigns_image;
    }

    public String getCampaigns_id() {
        return campaigns_id;
    }

    public void setCampaigns_id(String campaigns_id) {
        this.campaigns_id = campaigns_id;
    }
}