package com.netobjex.property_list.model;

import com.google.gson.annotations.SerializedName;
import com.netobjex.property_list.global.Constant;

import java.io.Serializable;

public class User implements Serializable {

    @SerializedName(Constant.Authorization)
    private String Authorization;

    public String getAuthorization() {
        return Authorization;
    }

    public void setAuthorization(String authorization) {
        Authorization = authorization;
    }
}
