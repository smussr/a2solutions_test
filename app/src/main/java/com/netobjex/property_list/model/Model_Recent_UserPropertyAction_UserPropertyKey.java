package com.netobjex.property_list.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Model_Recent_UserPropertyAction_UserPropertyKey {
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("propertyId")
    @Expose
    private String propertyId;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }
}
