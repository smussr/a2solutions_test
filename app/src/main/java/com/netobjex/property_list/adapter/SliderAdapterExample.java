package com.netobjex.property_list.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.netobjex.property_list.R;
import com.smarteist.autoimageslider.SliderViewAdapter;
import com.squareup.picasso.Picasso;


public class SliderAdapterExample extends SliderViewAdapter<SliderAdapterExample.SliderAdapterVH> {

    public Context context;

    public SliderAdapterExample(Context context) {
        this.context = context;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {
        viewHolder.textViewDescription.setText("This is slider_one item " + position);

        switch (position) {
            case 0:
                viewHolder.imageViewBackground.setImageDrawable(context.getResources().getDrawable(R.drawable.slider_one));
//                Picasso.with(context).load("https://images.pexels.com/photos/218983/pexels-photo-218983.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
//                        .placeholder(R.drawable.ic_launcher).into(viewHolder.imageViewBackground);

//                Glide.with(viewHolder.itemView)
//                        .load("https://images.pexels.com/photos/218983/pexels-photo-218983.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
//                        .into(viewHolder.imageViewBackground);
                break;
            case 1:
                viewHolder.imageViewBackground.setImageDrawable(context.getResources().getDrawable(R.drawable.slider_two));
//                Picasso.with(context).load("https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260")
//                        .placeholder(R.drawable.ic_launcher).into(viewHolder.imageViewBackground);

//                Glide.with(viewHolder.itemView)
//                        .load("https://images.pexels.com/photos/747964/pexels-photo-747964.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260")
//                        .into(viewHolder.imageViewBackground);
                break;
            case 2:
                viewHolder.imageViewBackground.setImageDrawable(context.getResources().getDrawable(R.drawable.slider_three));
//                Picasso.with(context).load("https://images.pexels.com/photos/929778/pexels-photo-929778.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
//                        .placeholder(R.drawable.ic_launcher).into(viewHolder.imageViewBackground);

//                Glide.with(viewHolder.itemView)
//                        .load("https://images.pexels.com/photos/929778/pexels-photo-929778.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
//                        .into(viewHolder.imageViewBackground);
                break;
            default:
                viewHolder.imageViewBackground.setImageDrawable(context.getResources().getDrawable(R.drawable.slider_one));
//                Picasso.with(context).load("https://images.pexels.com/photos/218983/pexels-photo-218983.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
//                        .placeholder(R.drawable.ic_launcher).into(viewHolder.imageViewBackground);

//                Glide.with(viewHolder.itemView)
//                        .load("https://images.pexels.com/photos/218983/pexels-photo-218983.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260")
//                        .into(viewHolder.imageViewBackground);
                break;

        }

    }

    @Override
    public int getCount() {
        //slider_one view count could be dynamic size
        return 3;
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;
        TextView textViewDescription;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider);
            this.itemView = itemView;
        }
    }
}