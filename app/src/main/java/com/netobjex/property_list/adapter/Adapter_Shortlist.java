package com.netobjex.property_list.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.netobjex.property_list.R;
import com.netobjex.property_list.model.Model_Shortlist;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class Adapter_Shortlist extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<Model_Shortlist> modelShortlists;
    private Context context;
    private boolean isLoadingAdded = false;

    public Adapter_Shortlist(Context context) {
        this.context = context;
        modelShortlists = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.cardview_shortlist_list, parent, false);
        viewHolder = new ProductVH(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Model_Shortlist result = modelShortlists.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                final ProductVH productVH = (ProductVH) holder;
                if (result.getThumbnail().isEmpty()) {
                    productVH.mImgPpt.setImageResource(R.drawable.ic_house_default);
                } else {
                    Picasso.with(context).load(result.getThumbnail()).placeholder(R.drawable.ic_house_default).into(productVH.mImgPpt);
                }
                productVH.mTvPptName.setText(result.getName());
                productVH.mTvPptPrice.setText(String.valueOf("US$ "+result.getPrice()));
                productVH.mTvPptType.setText(String.valueOf(result.getType()));
                productVH.mTvPptCapRate.setText(String.valueOf(result.getCapRate())+"% Cap Rate");
                productVH.mTvPptRooms.setText(String.valueOf(result.getNumberOfRooms())+" Rooms");

                break;
        }
    }

    @Override
    public int getItemCount() {
        return modelShortlists == null ? 0 : modelShortlists.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == modelShortlists.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }


    /*
   Helpers
   _________________________________________________________________________________________________
    */
    public String getlastproductid() {
        return modelShortlists.get(modelShortlists.size() - 2).getPropertyId();
    }

    public void add(Model_Shortlist r) {
        modelShortlists.add(r);
        notifyItemInserted(modelShortlists.size() - 1);
    }

    public void addAll(List<Model_Shortlist> modelRecommends) {
        for (Model_Shortlist result : modelRecommends) {
            add(result);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public void remove(Model_Shortlist r) {
        int position = modelShortlists.indexOf(r);
        if (position > -1) {
            modelShortlists.remove(position);
            notifyItemRemoved(position);
        }
    }

    public Model_Shortlist getItem(int position) {
        return modelShortlists.get(position);
    }

   /*
   View Holder
   _________________________________________________________________________________________________
    */

    protected class ProductVH extends RecyclerView.ViewHolder {
        ImageView mImgPpt;
        private TextView mTvPptName, mTvPptPrice, mTvPptType, mTvPptCapRate, mTvPptRooms;

        public ProductVH(View itemView) {
            super(itemView);
            mTvPptPrice = itemView.findViewById(R.id.item_price);
            mTvPptType = itemView.findViewById(R.id.item_type);
            mTvPptCapRate = itemView.findViewById(R.id.item_cap_rate);
            mTvPptRooms = itemView.findViewById(R.id.item_room_count);

            mTvPptName = itemView.findViewById(R.id.txt_ppt_name);
            mImgPpt = itemView.findViewById(R.id.img_ppt);
        }
    }


}