package com.netobjex.property_list.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.netobjex.property_list.R;
import com.netobjex.property_list.model.Model_Recent;
import com.netobjex.property_list.model.Model_Recommend;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class Adapter_Recent extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<Model_Recent> modelRecents;
    private Context context;
    private boolean isLoadingAdded = false;

    public Adapter_Recent(Context context) {
        this.context = context;
        modelRecents = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.cardview_recent_list, parent, false);
        viewHolder = new ProductVH(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Model_Recent result = modelRecents.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                final ProductVH productVH = (ProductVH) holder;
                if (result.getThumbnail().isEmpty()) {
                    productVH.mImgPpt.setImageResource(R.drawable.ic_house_default);
                } else{
                    Picasso.with(context).load(result.getThumbnail()).placeholder(R.drawable.ic_house_default).into(productVH.mImgPpt);
                }
                productVH.mTvPptName.setText(result.getName());
                break;
        }
    }

    @Override
    public int getItemCount() {
        return modelRecents == null ? 0 : modelRecents.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == modelRecents.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }


    /*
   Helpers
   _________________________________________________________________________________________________
    */
    public String getlastproductid() {
        return modelRecents.get(modelRecents.size() - 2).getPropertyId();
    }

    public void add(Model_Recent r) {
        modelRecents.add(r);
        notifyItemInserted(modelRecents.size() - 1);
    }

    public void addAll(List<Model_Recent> modelRecommends) {
        for (Model_Recent result : modelRecommends) {
            add(result);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }
    public void remove(Model_Recent r) {
        int position = modelRecents.indexOf(r);
        if (position > -1) {
            modelRecents.remove(position);
            notifyItemRemoved(position);
        }
    }

    public Model_Recent getItem(int position) {
        return modelRecents.get(position);
    }

   /*
   View Holder
   _________________________________________________________________________________________________
    */

    protected class ProductVH extends RecyclerView.ViewHolder {
        ImageView mImgPpt;
        private TextView mTvPptName;

        public ProductVH(View itemView) {
            super(itemView);
            mTvPptName = itemView.findViewById(R.id.txt_ppt_name);
            mImgPpt =  itemView.findViewById(R.id.img_ppt);
        }
    }


}