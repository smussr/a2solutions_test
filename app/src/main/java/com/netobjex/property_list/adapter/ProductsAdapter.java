package com.netobjex.property_list.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.netobjex.property_list.R;
import com.netobjex.property_list.model.Model_ProductList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class ProductsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<Model_ProductList> productLists;
    private Context context;
    private boolean isLoadingAdded = false;

    public ProductsAdapter(Context context) {
        this.context = context;
        productLists = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.cardview_recommend_list, parent, false);
        viewHolder = new ProductVH(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Model_ProductList result = productLists.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                final ProductVH productVH = (ProductVH) holder;
                Picasso.with(context).load(result.getCampaigns_image()).placeholder(R.drawable.ic_launcher).into(productVH.mImgPpt);
                productVH.mTvPptName.setText(result.getCampaigns_id());
                break;
        }
    }

    @Override
    public int getItemCount() {
        return productLists == null ? 0 : productLists.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == productLists.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }


    /*
   Helpers
   _________________________________________________________________________________________________
    */
    public String getlastproductid() {
        return productLists.get(productLists.size() - 2).getCampaigns_id();
    }

    public void add(Model_ProductList r) {
        productLists.add(r);
        notifyItemInserted(productLists.size() - 1);
    }

    public void addAll(List<Model_ProductList> modelProductLists) {
        for (Model_ProductList result : modelProductLists) {
            add(result);
        }
    }

    public Model_ProductList getItem(int position) {
        return productLists.get(position);
    }

   /*
   View Holder
   _________________________________________________________________________________________________
    */

    protected class ProductVH extends RecyclerView.ViewHolder {
        ImageView mImgPpt;
        private TextView mTvPptName;

        public ProductVH(View itemView) {
            super(itemView);
            mTvPptName = itemView.findViewById(R.id.txt_ppt_name);
            mImgPpt = (ImageView) itemView.findViewById(R.id.img_ppt);
        }
    }


}