package com.netobjex.property_list.global;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

//import com.netobjex.property_list.A_activity.MainActivity;
import com.netobjex.property_list.A_activity.Main_Act;

import java.io.PrintWriter;
import java.io.StringWriter;

public class GlobalExceptionHandler implements Thread.UncaughtExceptionHandler {

    Context context;

    public GlobalExceptionHandler(Context context) {
        // TODO Auto-generated constructor stub
        this.context = context;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        // TODO Auto-generated method stub
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        // sw.toString();

        Intent splashIntent = new Intent(context, Main_Act.class);
        splashIntent.putExtra("EXCEPTION", sw.toString());
        final PendingIntent intent = PendingIntent.getActivity(context, 0, splashIntent, Intent.FLAG_ACTIVITY_CLEAR_TASK);
        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 2000, intent);
        System.exit(2);
    }
}
