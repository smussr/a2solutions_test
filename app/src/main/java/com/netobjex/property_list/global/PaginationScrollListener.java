package com.netobjex.property_list.global;

import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

/**
 * Created by Sreekanth on 12/5/19.
 */

public abstract class PaginationScrollListener extends RecyclerView.OnScrollListener {

    LinearLayoutManager layoutManager;
    FloatingActionButton fab;

    public PaginationScrollListener(LinearLayoutManager layoutManager, FloatingActionButton fab) {
        this.layoutManager = layoutManager;
        this.fab = fab;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();
        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

//        FAB ===>
        if (fab != null) {
            if (dy > 0 && fab.getVisibility() == View.VISIBLE) {
                fab.hide();
            } else if (dy < 0 && fab.getVisibility() != View.VISIBLE) {
                fab.show();
            }
        }
//        <===

        if (!isLoading() && !isLastPage()) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                    && firstVisibleItemPosition >= 0
                    && totalItemCount >= getTotalPageCount()) {
                loadMoreItems();
            }
        }

    }

    protected abstract void loadMoreItems();

    public abstract int getTotalPageCount();

    public abstract boolean isLastPage();

    public abstract boolean isLoading();

}
