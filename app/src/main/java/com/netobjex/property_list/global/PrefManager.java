package com.netobjex.property_list.global;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {

    private SharedPreferences sharedPreference;
    private static PrefManager session;

    private PrefManager() {

    }

    public static PrefManager getPref(Context context) {
        if (session == null) {
            session = new PrefManager();
        }
        session.sharedPreference = context.getSharedPreferences(Constant.PREF_NAME, Context.MODE_PRIVATE);
        return session;
    }

    public static PrefManager getPref(Context context, String prefName) {
        if (session == null) {
            session = new PrefManager();
        }
        session.sharedPreference = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        return session;
    }

    public SharedPreferences getPref() {
        return sharedPreference;
    }

    public void putBoolean(String key, boolean value) {
        sharedPreference.edit().putBoolean(key, value).apply();
    }

    public void putString(String key, String value) {
        sharedPreference.edit().putString(key, value).apply();
    }

    public void putInt(String key, int value) {
        sharedPreference.edit().putInt(key, value).apply();
    }

    public boolean getBoolean(String key) {
        return sharedPreference.getBoolean(key, false);
    }

    public String getString(String key) {
        return sharedPreference.getString(key, "");
    }

    public int getInt(String key) {
        return sharedPreference.getInt(key, 0);
    }

    public void clear(String key) {
        sharedPreference.edit().remove(key).apply();
    }

    public void clearAll() {
        sharedPreference.edit().clear().apply();
    }

    public boolean contains(String key) {
        return sharedPreference.contains(key);
    }


}
