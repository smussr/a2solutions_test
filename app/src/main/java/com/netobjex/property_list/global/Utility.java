package com.netobjex.property_list.global;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

public class Utility {
    public static final String DATE_FORMAT_SERVER_DATE_LONG = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_SERVER_DATE_SHORT = "yyyy-MM-dd";


    public static boolean isBlank(EditText edt) {
        if (edt == null
                || (edt != null && TextUtils.isEmpty(edt.getText().toString()
                .trim()))) {
            return true;
        }
        return false;
    }

    public static final int TOAST_DURATION = Toast.LENGTH_SHORT;

    private void showInternetMsg(Context activity) {
        Toast.makeText(activity, "No internet connection found. Please check your connection and try again", TOAST_DURATION).show();
    }

    public static boolean isInternetConnected(Context activity) {
        if (activity != null) {
            ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netinfo = cm.getActiveNetworkInfo();
            if (netinfo != null && netinfo.isConnectedOrConnecting()) {
                android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                return (mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting());
            } else
                return false;
        } else {
            return false;
        }
    }

    public static void showLogE(Class cl, String msg) {
        if (Constant.SHOW_LOG_E)
            Log.e(cl.getSimpleName(), msg);
    }

    public static void showLog(Class cl, String msg) {
        if (Constant.SHOW_LOG_I)
            Log.i(cl.getSimpleName(), msg);
    }

    public static void showToast(Context context, String msg) {

        if (msg != null)
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    private static ProgressDialog progressDialog;

    public static void showProgressDialog(Activity mActivity, String message,
                                          DialogInterface.OnCancelListener cancelListener) {
        // TODO Auto-generated method stub
        progressDialog = ProgressDialog.show(mActivity, null, message, true,
                true, cancelListener);
        progressDialog.setCanceledOnTouchOutside(false);
    }

    public static void dismissProgressDialog() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    public static String getDateForWebService(Calendar calendar) {
        // TODO Auto-generated method stub
        if (calendar != null) {
            return DateFormat.format("yyyy-MM-dd", calendar).toString();
        }
        return "";
    }

    public static Drawable getAndroidDrawable(Context context,
                                              String pDrawableName) {
        int resourceId = context.getResources().getIdentifier(pDrawableName,
                "drawable", context.getPackageName());
        if (resourceId == 0) {
            return null;
        } else {
            return context.getResources().getDrawable(resourceId);
        }
    }

    public static boolean isEmpty(String text) {
        // TODO Auto-generated method stub
        if (text == null || (text != null && TextUtils.isEmpty(text.trim()))) {
            return true;
        }
        return false;
    }

    //----------------------------------Util Functions--------------------------------------------//
    public static void hideKeyboard(Activity activity) {
        if (activity != null) {
            hideKeyboard(activity, null);
        }
    }

    public static void hideKeyboard(Activity activity, View view) {
        if (activity != null) {
            InputMethodManager inputManager = (InputMethodManager) activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);

            if (view == null) {
                view = activity.getCurrentFocus();
            }

            if (view != null) {
                inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
            }

        }
    }
}
