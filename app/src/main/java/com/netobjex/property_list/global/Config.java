package com.netobjex.property_list.global;

import android.widget.Toast;

public class Config {
    public static final int TOAST_DURATION = Toast.LENGTH_SHORT;

    public static final long SPLASH_TIMEOUT = 2500;
    public static final long SPLASH_TIMEOUT_SMALL = 1500;
}
