package com.netobjex.property_list.global;

import android.widget.Toast;

public class Constant {
    public static final String IN_STATUS = "STATUS";
    public static final String IN_STATUS_S = "status";
    public static final String IN_ERROR_S = "error";

    public static final String dateFormatForServer = "yyyy-MM-dd";
    public static final String DATE_FORMAT_SERVER_DATE_TIME = "EEEE dd MMM,yyyy kk:mm";


    public static final int TOAST_DURATION = Toast.LENGTH_LONG;
    public static final boolean SHOW_TOAST = true;
    public static final boolean SHOW_LOG_I = true;
    public static final boolean SHOW_LOG_E = true;

    public static final String BUILD_DATE = "24 July 2019";

    // Prefrences
    public static final String PREF_HIDE_USER = "pref_hide_user";
    public static final String PREF_NAME = "PL";
    public static final String PREF_IS_FIRST_TIME = "is_first_time";
    public static final String PREF_KEY_SECURITY = "pref_security_key";
    public static final String PREF_ACCESS_TOKEN = "pref_access_token";
    public static final String PREF_LAT= "pref_lat";
    public static final String PREF_LNG= "pref_lng";
    public static final String PREF_UID = "pref_user_id";

    public static final String TRIP_ACTION_DATA = "action_data";



    /*===========================================================================================>*/
    //    LOGIN DATA
    public static final String Authorization = "Authorization";

    public static final String password = "password";
    public static final String userDetails = "userDetails";
    public static final String userId = "user_id"; //userdata fo login data (Declared as JSONArray)
    public static final String userType = "user_type";
    public static final String name = "name";
    public static final String phoneNo = "phone_no";
    public static final String address = "address";


}
