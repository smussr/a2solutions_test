package com.netobjex.property_list;

import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.util.Log;


import com.netobjex.property_list.api.Response_User_Info;
import com.netobjex.property_list.db.DBHelper;
import com.netobjex.property_list.global.Constant;
import com.netobjex.property_list.global.GlobalExceptionHandler;
import com.netobjex.property_list.global.PrefManager;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class App extends Application {

    private static App appInstance;
    private Response_User_Info user;

    @Override
    public void onCreate() {
        appInstance = this;
        user = getUser();
        super.onCreate();

        if (true) {
            Thread.setDefaultUncaughtExceptionHandler(new GlobalExceptionHandler(this));
        }
    }

    public static synchronized App instance() {
        return appInstance;
    }

    public void setUser(Response_User_Info usr) {
        user = usr;
    }

    public Response_User_Info getUser() {
        Log.i("USER", "getUser: " + user);
        if (user == null) {
            user = DBHelper.instance().getUser();
            Log.i("DRIVER", "getDriver: driver is null so getting from database" + user);
        }
        return user;
    }

    public void logOutUser() {
        user = null;
    }

    public void setAccessToken(String instructorType) {
        PrefManager.getPref(this).putString(Constant.PREF_ACCESS_TOKEN, instructorType);
    }

    public String getAccessToken() {
        return PrefManager.getPref(this).getString(Constant.PREF_ACCESS_TOKEN);
    }


    public void setUid(String userID) {
        PrefManager.getPref(this).putString(Constant.PREF_UID, userID);
    }

    public String getUid() {
        return PrefManager.getPref(this).getString(Constant.PREF_UID);
    }


    public void setHasRunBefore(boolean hasRunBefore) {
        PrefManager.getPref(this).putBoolean(Constant.PREF_IS_FIRST_TIME, hasRunBefore);
    }

    public boolean getHasRunBefore() {
        return PrefManager.getPref(this).getBoolean(Constant.PREF_IS_FIRST_TIME);
    }

    public void setLat(String lat) {
        PrefManager.getPref(this).putString(Constant.PREF_LAT, lat);
    }

    public String getLat() {
        return PrefManager.getPref(this).getString(Constant.PREF_LAT);
    }

    public void setLng(String lng) {
        PrefManager.getPref(this).putString(Constant.PREF_LNG, lng);
    }

    public String getLng() {
        return PrefManager.getPref(this).getString(Constant.PREF_LNG);
    }


    public boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }


    //Retrofit
    public static final String BASE_URL = BuildConfig.BASE_URL;
    private static Retrofit retrofit = null;

    public static Retrofit getClient_() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }
        return retrofit;
    }

}
