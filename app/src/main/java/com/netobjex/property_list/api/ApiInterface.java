package com.netobjex.property_list.api;


import com.google.gson.JsonArray;
import com.netobjex.property_list.model.Model_Mylist;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {
    String POST_LOGIN = "api/user/login";
    String GET_INFO = "api/user/info";
    String GET_RECOMMEND = "api/pub/property/recommend";
    String GET_RECENT = "api/property/visited/list";
    String GET_SHORTLIST = "api/property/shortlist";

    String POST_SHORTLIST = "api/properties";

//    String GET_TICKETS = "api/user/info";


    @POST(POST_LOGIN)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<Response_Login> getCredentials(
            @Body HashMap<String, String> body);

    @GET(GET_INFO)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<Response_User_Info> getInfo(
            @Header("Authorization") String authorization);

    @GET(GET_RECOMMEND)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<JsonArray> getRecommend(
            @Header("Authorization") String authorization,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("type") String type, //type=sale or type=rent
            @Query("uid") String uid
    );

    @GET(GET_RECENT)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<JsonArray> getRecent(
            @Header("Authorization") String authorization,
            @Query("type") String type, //type=sale or type=rent
            @Query("userId") String uid
    );

    @GET(GET_SHORTLIST)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<JsonArray> getShortlist(
            @Header("Authorization") String authorization
    );


    @POST(POST_SHORTLIST)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<Model_Mylist> getMyList(
            @Header("Authorization") String authorization,
            @Body HashMap<String, String> body);



/*
    //    @FormUrlEncoded
    @POST(GET_TICKETS)
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<Response_ProductList> getTickets(
            @Header("Authorization") String authorization,
            @Field("category_id") String category_id,
            @Field("language") String language);
*/


//    @FormUrlEncoded
//    @POST(GET_TICKETS)
//    Call<JSONObject> getTickets(
//            @Field("access_token") String access_token,
//            @Field("category_id") String category_id,
//            @Field("language") String language);

}