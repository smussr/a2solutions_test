package com.netobjex.property_list.A_fragments;

import android.util.Log;
import android.view.View;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.netobjex.property_list.App;
import com.netobjex.property_list.BaseFragment;
import com.netobjex.property_list.R;
import com.netobjex.property_list.adapter.Adapter_Recent;
import com.netobjex.property_list.adapter.Adapter_Recommend;
import com.netobjex.property_list.adapter.Adapter_Shortlist;
import com.netobjex.property_list.api.ApiInterface;
import com.netobjex.property_list.model.Model_Recent;
import com.netobjex.property_list.model.Model_Recommend;
import com.netobjex.property_list.model.Model_Shortlist;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Fragment_Shortlists extends BaseFragment {


    RecyclerView rv_shortlist;
    Adapter_Shortlist adapter_shortlist;
    LinearLayoutManager linearLayoutManager_Shortlist;
    List<Model_Shortlist> modelShortlists = new ArrayList<>();

    private SwipeRefreshLayout swipeContainer;

    public Fragment_Shortlists() {
        setContentView(R.layout.fragment_shortlists);
    }

    @Override
    public void initVariable() {

    }

    @Override
    public void initializeView(View v) {
        rv_shortlist = (RecyclerView) v.findViewById(R.id.rv_recent_shortlist);
        adapter_shortlist = new Adapter_Shortlist(getContext());
        linearLayoutManager_Shortlist = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        rv_shortlist.setLayoutManager(linearLayoutManager_Shortlist);
        rv_shortlist.setItemAnimator(new DefaultItemAnimator());
        rv_shortlist.setAdapter(adapter_shortlist);

        swipeContainer = v.findViewById(R.id.swipe_container);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimaryDark), getResources().getColor(R.color.colorAccent));
        swipeContainer.setDistanceToTriggerSync(15);
        swipeContainer.setSize(SwipeRefreshLayout.DEFAULT);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (adapter_shortlist != null && adapter_shortlist.getItemCount() > 0) {
                    adapter_shortlist.clear();
                }
                loadData();
            }
        });
    }

    @Override
    public void postInitView() {

    }

    @Override
    public void addAdapter() {
    }

    @Override
    public void loadData() {
        getShortlists(App.instance().getAccessToken());
    }

    public void getShortlists(String authorization) {
        showProgressDialog();
        ApiInterface apiService = App.getClient_().create(ApiInterface.class);
        Call<JsonArray> call = apiService.getShortlist(authorization);
        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                dismissProgressDialog();
                swipeContainer.setRefreshing(false);
                try {
                    String teamString = response.body().toString();
                    Type listType = new TypeToken<List<Model_Shortlist>>() {
                    }.getType();
                    modelShortlists = getTeamListFromJson(teamString, listType);
                    adapter_shortlist.addAll(modelShortlists);

                } catch (Exception e) {
                    Log.d("onResponse", "There is an error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                dismissProgressDialog();
                swipeContainer.setRefreshing(false);
                Log.d("onFailure", t.toString());
            }
        });
    }


    public static <T> List<T> getTeamListFromJson(String jsonString, Type type) {
        if (!isValid(jsonString)) {
            return null;
        }
        return new Gson().fromJson(jsonString, type);
    }

    public static boolean isValid(String json) {
        try {
            new JsonParser().parse(json);
            return true;
        } catch (JsonSyntaxException jse) {
            return false;
        }
    }

}
