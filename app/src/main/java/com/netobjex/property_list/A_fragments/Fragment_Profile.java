package com.netobjex.property_list.A_fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.netobjex.property_list.A_activity.Login_Act;
import com.netobjex.property_list.App;
import com.netobjex.property_list.BaseFragment;
import com.netobjex.property_list.R;
import com.netobjex.property_list.api.Response_User_Info;
import com.netobjex.property_list.db.DBHelper;
import com.netobjex.property_list.global.Constant;
import com.netobjex.property_list.global.PrefManager;
import com.squareup.picasso.Picasso;

import java.util.HashMap;


public class Fragment_Profile extends BaseFragment {

    TextView pProfileName, pProfileLocation;
    ImageView pProfilePicture;
    Response_User_Info userInfo;

    LinearLayout PL_Settings, PL_Feedback, PL_Help, PL_LogOut;

    public Fragment_Profile() {
        setContentView(R.layout.fragment_profile);
    }

    @Override
    public void initVariable() {

    }

    @Override
    public void initializeView(View v) {
        pProfilePicture = v.findViewById(R.id.profile_image);
        pProfileName = v.findViewById(R.id.profile_name);
        pProfileLocation = v.findViewById(R.id.profile_location);

        PL_Settings = v.findViewById(R.id.pl_settings);
        PL_Feedback = v.findViewById(R.id.pl_feedback);
        PL_Help = v.findViewById(R.id.pl_help);
        PL_LogOut = v.findViewById(R.id.pl_logout);

        userInfo = new Response_User_Info();

        PL_LogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutPopUp();
            }
        });
    }

    @Override
    public void postInitView() {

    }

    @Override
    public void addAdapter() {
    }

    @Override
    public void loadData() {
        userInfo = App.instance().getUser();

        if (userInfo.getImage().isEmpty()) {
            pProfilePicture.setImageResource(R.drawable.bottom_ic_profile_black);
        } else {
            Picasso.with(getContext()).load(userInfo.getImage()).placeholder(R.drawable.bottom_ic_profile_black).into(pProfilePicture);
        }
        pProfileName.setText(userInfo.getFullName());
        pProfileLocation.setText(userInfo.getCountry());
    }


    private void logoutPopUp() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setMessage("Do you wish to log out?");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        doLogOut();
                        showProgressDialog();
                    }
                });

        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void doLogOut() {
//        if (hasInternetConnection()) {

        SharedPreferences sharedPreferences = PrefManager.getPref(getContext()).getPref();
        //getting status of instruction screen and setting once again after clear
        boolean hide = sharedPreferences.getBoolean(Constant.PREF_HIDE_USER, false);
        PrefManager.getPref(getContext()).clearAll();
        sharedPreferences.edit().putBoolean(Constant.PREF_HIDE_USER, hide).apply();

        DBHelper.instance().logOutUser_(App.instance().getUser().getUid());
        App.instance().logOutUser();
        startActivity(new Intent(mActivity, Login_Act.class));
        mActivity.finish();
        dismissProgressDialog();


//        } else {
//            showInternetMsg();
//        }
    }

}
