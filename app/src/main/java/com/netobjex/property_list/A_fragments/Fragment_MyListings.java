package com.netobjex.property_list.A_fragments;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.netobjex.property_list.A_activity.Login_Act;
import com.netobjex.property_list.A_activity.Main_Act;
import com.netobjex.property_list.App;
import com.netobjex.property_list.BaseFragment;
import com.netobjex.property_list.R;
import com.netobjex.property_list.adapter.Adapter_Mylist;
import com.netobjex.property_list.adapter.Adapter_Shortlist;
import com.netobjex.property_list.api.ApiInterface;
import com.netobjex.property_list.api.Response_Login;
import com.netobjex.property_list.global.Constant;
import com.netobjex.property_list.global.PrefManager;
import com.netobjex.property_list.model.Model_Mylist;
import com.netobjex.property_list.model.Model_Shortlist;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class Fragment_MyListings extends BaseFragment {


    RecyclerView rv_mylist;
    Adapter_Mylist adapter_mylist;
    LinearLayoutManager linearLayoutManager_Mylist;
    List<Model_Mylist.Content> modelMylists = new ArrayList<>();

    private SwipeRefreshLayout swipeContainer;

    public Fragment_MyListings() {
        setContentView(R.layout.fragment_my_listings);
    }

    @Override
    public void initVariable() {

    }

    @Override
    public void initializeView(View v) {
        rv_mylist = (RecyclerView) v.findViewById(R.id.rv_recent_mylist);
        adapter_mylist = new Adapter_Mylist(getContext());
        linearLayoutManager_Mylist = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        rv_mylist.setLayoutManager(linearLayoutManager_Mylist);
        rv_mylist.setItemAnimator(new DefaultItemAnimator());
        rv_mylist.setAdapter(adapter_mylist);

        swipeContainer = v.findViewById(R.id.swipe_container);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimaryDark), getResources().getColor(R.color.colorAccent));
        swipeContainer.setDistanceToTriggerSync(15);
        swipeContainer.setSize(SwipeRefreshLayout.DEFAULT);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (adapter_mylist != null && adapter_mylist.getItemCount() > 0) {
                    adapter_mylist.clear();
                }
                loadData();
            }
        });

    }

    @Override
    public void postInitView() {

    }

    @Override
    public void addAdapter() {
    }

    @Override
    public void loadData() {
        getMylists(App.instance().getAccessToken());
    }


    public void getMylists(String authorization) {
        showProgressDialog();
        ApiInterface apiService = App.getClient_().create(ApiInterface.class);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("offset", "1");
        params.put("pageNumber", "1");
        params.put("pageSize", "12");
        params.put("paged", "true");
        params.put("sort.sorted", "true");
        params.put("sort.unsorted", "false");
        params.put("unpaged", "false");
        Call<Model_Mylist> call = apiService.getMyList(authorization, params);

        call.enqueue(new Callback<Model_Mylist>() {
            @Override
            public void onResponse(Call<Model_Mylist> call, Response<Model_Mylist> response) {
                dismissProgressDialog();
                swipeContainer.setRefreshing(false);
                int statusCode = response.code();
                if (response.body() != null) {
                    try {
                        String teamString = response.body().getContent().toString();
                        Type listType = new TypeToken<List<Model_Mylist>>() {
                        }.getType();
                        modelMylists = response.body().getContent();
                        adapter_mylist.addAll(modelMylists);

                    } catch (Exception e) {
                        Log.d("onResponse", "There is an error");
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Model_Mylist> call, Throwable t) {
                dismissProgressDialog();
                swipeContainer.setRefreshing(false);
                Log.e(TAG, t.toString());
            }
        });
    }


    public static <T> List<T> getTeamListFromJson(String jsonString, Type type) {
        if (!isValid(jsonString)) {
            return null;
        }
        return new Gson().fromJson(jsonString, type);
    }

    public static boolean isValid(String json) {
        try {
            new JsonParser().parse(json);
            return true;
        } catch (JsonSyntaxException jse) {
            return false;
        }
    }
}
