package com.netobjex.property_list.A_fragments;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.netobjex.property_list.App;
import com.netobjex.property_list.BaseFragment;
import com.netobjex.property_list.R;
import com.netobjex.property_list.adapter.Adapter_Recent;
import com.netobjex.property_list.adapter.Adapter_Recommend;
import com.netobjex.property_list.adapter.SliderAdapterExample;
import com.netobjex.property_list.api.ApiInterface;
import com.netobjex.property_list.model.LocationTrack;
import com.netobjex.property_list.model.Model_Recent;
import com.netobjex.property_list.model.Model_Recommend;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_Home extends BaseFragment {

    private static final String TAG = "Fragment_Home";
    SliderView sliderView;

    RecyclerView rv_recommend, rv_recent;
    Adapter_Recommend adapter_recommend;
    Adapter_Recent adapter_recent;
    LinearLayoutManager linearLayoutManager_Recommended, linearLayoutManager_Recent;
    List<Model_Recommend> modelRecommendList = new ArrayList<>();
    List<Model_Recent> modelRecentList = new ArrayList<>();

    private SwipeRefreshLayout swipeContainer;

    LocationTrack locationTrack;

    public Fragment_Home() {
        setContentView(R.layout.fragment_home);
    }

    @Override
    public void initVariable() {
    }

    @Override
    public void initializeView(View v) {
        sliderView = v.findViewById(R.id.imageSlider);
        sliderView.setSliderAdapter(new SliderAdapterExample(getActivity()));
        sliderView.startAutoCycle();
        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);

        swipeContainer = v.findViewById(R.id.swipe_container);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimaryDark), getResources().getColor(R.color.colorAccent));
        swipeContainer.setDistanceToTriggerSync(15);
        swipeContainer.setSize(SwipeRefreshLayout.DEFAULT);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (adapter_recommend != null && adapter_recommend.getItemCount() > 0) {
                    adapter_recommend.clear();
                }
                if (adapter_recent != null && adapter_recent.getItemCount() > 0) {
                    adapter_recent.clear();
                }
                loadData();
            }
        });


        rv_recommend = (RecyclerView) v.findViewById(R.id.rv_recommended_properties);
        adapter_recommend = new Adapter_Recommend(getContext());
        linearLayoutManager_Recommended = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rv_recommend.setLayoutManager(linearLayoutManager_Recommended);
        rv_recommend.setItemAnimator(new DefaultItemAnimator());
        rv_recommend.setAdapter(adapter_recommend);


        rv_recent = (RecyclerView) v.findViewById(R.id.rv_recent_visited);
        adapter_recent = new Adapter_Recent(getContext());
        linearLayoutManager_Recent = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        rv_recent.setLayoutManager(linearLayoutManager_Recent);
        rv_recent.setItemAnimator(new DefaultItemAnimator());
        rv_recent.setAdapter(adapter_recent);
    }

    @Override
    public void postInitView() {
    }

    @Override
    public void addAdapter() {
    }

    @Override
    public void loadData() {
        getRecommendList(App.instance().getAccessToken(), App.instance().getLat(), App.instance().getLng(), "sale", App.instance().getUid());
        getRecentList(App.instance().getAccessToken(), "sale", App.instance().getUid());
    }

    public void getRecommendList(String authorization, String lat, String lng, String type, String uid) {
        locationTrack = new LocationTrack(getContext());
        if (locationTrack.canGetLocation()) {
            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();
        } else {
            locationTrack.showSettingsAlert();
        }

        showProgressDialog();
        ApiInterface apiService = App.getClient_().create(ApiInterface.class);
        Call<JsonArray> call = apiService.getRecommend(authorization, String.valueOf(locationTrack.getLatitude()), String.valueOf(locationTrack.getLongitude()), type, uid);
        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                dismissProgressDialog();
                swipeContainer.setRefreshing(false);
                try {
                    String teamString = response.body().toString();
                    Type listType = new TypeToken<List<Model_Recommend>>() {
                    }.getType();
                    modelRecommendList = getTeamListFromJson(teamString, listType);
                    adapter_recommend.addAll(modelRecommendList);

                } catch (Exception e) {
                    Log.d("onResponse", "There is an error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                dismissProgressDialog();
                swipeContainer.setRefreshing(false);
                Log.d("onFailure", t.toString());
            }
        });
    }

    public void getRecentList(String authorization, String type, String uid) {
        showProgressDialog();
        ApiInterface apiService = App.getClient_().create(ApiInterface.class);
        Call<JsonArray> call = apiService.getRecent(authorization, type, uid);
        call.enqueue(new Callback<JsonArray>() {
            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                dismissProgressDialog();
                swipeContainer.setRefreshing(false);
                try {
                    String teamString = response.body().toString();
                    Type listType = new TypeToken<List<Model_Recent>>() {
                    }.getType();
                    modelRecentList = getTeamListFromJson(teamString, listType);
                    adapter_recent.addAll(modelRecentList);

                } catch (Exception e) {
                    Log.d("onResponse", "There is an error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                dismissProgressDialog();
                swipeContainer.setRefreshing(false);
                Log.d("onFailure", t.toString());
            }
        });
    }


    public static <T> List<T> getTeamListFromJson(String jsonString, Type type) {
        if (!isValid(jsonString)) {
            return null;
        }
        return new Gson().fromJson(jsonString, type);
    }

    public static boolean isValid(String json) {
        try {
            new JsonParser().parse(json);
            return true;
        } catch (JsonSyntaxException jse) {
            return false;
        }
    }

}
