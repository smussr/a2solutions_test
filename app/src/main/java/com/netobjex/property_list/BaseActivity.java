package com.netobjex.property_list;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.netobjex.property_list.global.Utility;

import static com.netobjex.property_list.global.Constant.TOAST_DURATION;


/**
 * Created by Sreekanth Unnikrishnan
 */
public abstract class BaseActivity extends AppCompatActivity {


    protected abstract void initVariable();

    protected abstract void initView();

    protected abstract void postInitView();

    protected abstract void addAdapter();

    protected abstract void loadData();

    private ProgressDialog progressDialog;

//    protected Toolbar toolbar;

    @Override
    public void setContentView(int layoutResID) {
        try {
            super.setContentView(layoutResID);

            initVariable();
            initView();
            postInitView();
            addAdapter();
            loadData();

        } catch (Exception e) {
            e.printStackTrace();
            showToast(e.toString());
            Utility.showLogE(BaseActivity.class, e.toString());
        }
    }

    protected final View links(int resId) {
        return findViewById(resId);
    }

    protected final void setToolbar(int id) {
    }

    protected final void setNavigationUp() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    protected final void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    protected final void showToast(String msg) {
        Toast.makeText(this, msg, TOAST_DURATION).show();
    }

    protected final void showApiErrorMsg(String errorMsg) {
        showToast(errorMsg);
    }

    protected boolean hasInternetConnection() {
        return Utility.isInternetConnected(this);
    }

    protected final void showInternetMsg() {
        showToast(getResources().getString(R.string.message_no_internet_found));
    }
    public AlertDialog.Builder buildDialog_withExit(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("No Internet Connection");
        builder.setMessage("Check for your internet connection and try again later");
        builder.setCancelable(false);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                finish();
            }
        });
        return builder;
    }
    public AlertDialog.Builder buildDialog(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("No Internet Connection");
        builder.setMessage("Check for your internet connection and try again later");
        builder.setCancelable(false);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder;
    }


    protected final void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Please Wait...");
        }
        progressDialog.show();
    }

    protected final ProgressDialog showProgressDialogAsNotCancellable() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Please Wait...");
        }
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        return progressDialog;
    }

    protected void dismissProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
