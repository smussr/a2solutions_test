//package com.netobjex.property_list.A_activity;
//
//import android.os.Bundle;
//import android.os.Handler;
//import android.util.Log;
//import android.widget.Toast;
//
//
//import androidx.recyclerview.widget.DefaultItemAnimator;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.netobjex.property_list.App;
//import com.netobjex.property_list.BaseActivity;
//import com.netobjex.property_list.R;
//import com.netobjex.property_list.adapter.ProductsAdapter;
//import com.netobjex.property_list.api.ApiInterface;
//import com.netobjex.property_list.api.Response_User_Info;
//import com.netobjex.property_list.model.Model_ProductList;
//
//import java.util.List;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//public class MainActivity extends BaseActivity {
//    private static final String TAG = MainActivity.class.getSimpleName();
//
//    RecyclerView rv;
//    ProductsAdapter adapter;
//    LinearLayoutManager linearLayoutManager;
//    List<Model_ProductList> productList;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        rv = (RecyclerView) findViewById(R.id.orders_rv_recycler_view);
//        adapter = new ProductsAdapter(MainActivity.this);
//        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//        rv.setLayoutManager(linearLayoutManager);
//        rv.setItemAnimator(new DefaultItemAnimator());
//        rv.setAdapter(adapter);
//
//    }
//
//    @Override
//    protected void initVariable() {
//
//    }
//
//    @Override
//    protected void initView() {
//
//    }
//
//    @Override
//    protected void postInitView() {
//
//    }
//
//    @Override
//    protected void addAdapter() {
//
//    }
//
//    @Override
//    protected void loadData() {
//        if (hasInternetConnection()) {
//            getInfo();
//        } else showInternetMsg();
//    }
//
//    private void getInfo() {
//        showProgressDialog();
////        String category_id = "0";
////        String language = "1";
//        ApiInterface apiService = App.getClient_().create(ApiInterface.class);
//        Call<Response_User_Info> call = apiService.getInfo(
//                App.instance().getAccessToken());
//
//        call.enqueue(new Callback<Response_User_Info>() {
//            @Override
//            public void onResponse(Call<Response_User_Info> call, Response<Response_User_Info> response) {
//                dismissProgressDialog();
//                int statusCode = response.code();
//                if (response.body() != null) {
//                    Toast.makeText(MainActivity.this, String.valueOf(response.body()), Toast.LENGTH_SHORT).show();
////                    productList = response.body().getoData_().getModelProductLists();
////                    adapter.addAll(productList);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Response_User_Info> call, Throwable t) {
//                dismissProgressDialog();
//                Log.e(TAG, t.toString());
//            }
//        });
//    }
//
//    /*
//    *
//    private void getTickets() {
//        showProgressDialog();
//        String category_id = "0";
//        String language = "1";
//        ApiInterface apiService = App.getClient_().create(ApiInterface.class);
//        Call<Response_ProductList> call = apiService.getTickets(
//                App.instance().getAccessToken(),
//                category_id,
//                language);
//
//        call.enqueue(new Callback<Response_ProductList>() {
//            @Override
//            public void onResponse(Call<Response_ProductList> call, Response<Response_ProductList> response) {
//                dismissProgressDialog();
//                int statusCode = response.code();
//                if (response.body() != null) {
//                    productList = response.body().getoData_().getModelProductLists();
//                    adapter.addAll(productList);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Response_ProductList> call, Throwable t) {
//                dismissProgressDialog();
//                Log.e(TAG, t.toString());
//            }
//        });
//    }
//    */
//
//    boolean doubleBackToExitPressedOnce = false;
//
//    @Override
//    public void onBackPressed() {
//        if (doubleBackToExitPressedOnce) {
//            finish();
//        }
//        this.doubleBackToExitPressedOnce = true;
//        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                doubleBackToExitPressedOnce = false;
//            }
//        }, 2000);
//
//
//    }
//}