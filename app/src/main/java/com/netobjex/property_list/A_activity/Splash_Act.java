package com.netobjex.property_list.A_activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.netobjex.property_list.App;
import com.netobjex.property_list.BaseActivity;
import com.netobjex.property_list.R;
import com.netobjex.property_list.global.Config;


public class Splash_Act extends BaseActivity {

    ImageView img_logo;
    Animation popUpAnim;


    private Handler handler;
    private Runnable runnable;
    private String TAG = "Splash_Act";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_);
    }

    @Override
    protected void initVariable() {
        popUpAnim = AnimationUtils.loadAnimation(this, R.anim.splash_logo_anim);
    }

    @Override
    protected void initView() {
        img_logo = findViewById(R.id.logo);
    }

    @Override
    protected void postInitView() {
        img_logo.startAnimation(popUpAnim);
//        if ((Boolean) Utility.isInternetConnected(Splash_Act.this)) {
            startHandler(Config.SPLASH_TIMEOUT);
//        } else {
//            buildDialog_withExit(Splash_Act.this).show();
//        }
    }

    @Override
    protected void addAdapter() {

    }

    @Override
    protected void loadData() {

    }


//    private void getSecurityKey() {
//        WebUtils.newWebServiceRequest(new WebServiceModel(WebConstant.GET_KEY, WebConstant.WebService.KEY), this);
//    }


    private void startHandler(long timeInMillis) {
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (App.instance().getHasRunBefore()) {
//                    gotoLoginActivity();
                    if (App.instance().getUser() != null) {
                        goToMainActivity();
                    } else {
                        gotoLoginActivity();
                    }
                } else {
                    goToWelcomeSlider();
                    App.instance().setHasRunBefore(true);
                }
            }
        };

        handler.postDelayed(runnable, timeInMillis);
    }

    private void goToWelcomeSlider() {
        Intent i = new Intent(Splash_Act.this, WelcomeSlider_Act.class);
        startActivity(i);
        finish();
    }

    private void goToMainActivity() {
        Intent i = new Intent(Splash_Act.this, Main_Act.class);
        startActivity(i);
        finish();
    }

    private void gotoLoginActivity() {
        Intent i = new Intent(Splash_Act.this, Login_Act.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (handler != null)
            handler.removeCallbacks(runnable);
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
