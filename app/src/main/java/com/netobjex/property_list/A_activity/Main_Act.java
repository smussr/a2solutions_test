package com.netobjex.property_list.A_activity;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.netobjex.property_list.A_fragments.Fragment_Home;
import com.netobjex.property_list.A_fragments.Fragment_MyListings;
import com.netobjex.property_list.A_fragments.Fragment_Profile;
import com.netobjex.property_list.A_fragments.Fragment_Shortlists;
import com.netobjex.property_list.App;
import com.netobjex.property_list.BaseActivity;
import com.netobjex.property_list.R;
import com.netobjex.property_list.api.ApiInterface;
import com.netobjex.property_list.api.Response_User_Info;
import com.netobjex.property_list.db.DBHelper;
import com.netobjex.property_list.model.LocationTrack;


import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class Main_Act extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = Main_Act.class.getSimpleName();


    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<String>();

    private final static int ALL_PERMISSIONS_RESULT = 101;
    LocationTrack locationTrack;


    BottomNavigationView navigationView;

    private DrawerLayout drawerLayout;
    private int mCurrentMenu;
    private ProgressBar toolbarProgressBar;
    private Fragment_Home fragmentHome;
    private Fragment_Shortlists fragmentShortlists;
    private Fragment_MyListings fragmentMyListings;
    private Fragment_Profile fragmentProfile;

    public Fragment getSelFragment() {
        return selFragment;
    }

    private Fragment selFragment;

    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_);

        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0)
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }
//        locationTrack = new LocationTrack(Main_Act.this);
//        if (locationTrack.canGetLocation()) {
//            double longitude = locationTrack.getLongitude();
//            double latitude = locationTrack.getLatitude();
//            Toast.makeText(getApplicationContext(), "Longitude:" + Double.toString(longitude) + "\nLatitude:" + Double.toString(latitude), Toast.LENGTH_SHORT).show();
//        } else {
//            locationTrack.showSettingsAlert();
//        }
    }

    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<>();
        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }
        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }
                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(Main_Act.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }





    /*
     *
     *
     * */

    @Override
    protected void initVariable() {
        navigationView = findViewById(R.id.navigation);
        navigationView.setOnNavigationItemSelectedListener(this);
//        loadFragment(new Fragment_Home());

        navigationView.getMenu().getItem(0).setChecked(true);
        onNavigationItemSelected(navigationView.getMenu().getItem(0));
    }

    @Override
    protected void initView() {
        setToolbar(R.id.toolbar);
        toolbarProgressBar = (ProgressBar) links(R.id.toolbar_progress);
        drawerLayout = (DrawerLayout) links(R.id.drawer_layout);
    }

    @Override
    protected void postInitView() {

    }

    @Override
    protected void addAdapter() {

    }

    @Override
    protected void loadData() {
        getInfo();
    }

//    private boolean loadFragment(Fragment fragment) {
//        if (fragment != null) {
//            getSupportFragmentManager()
//                    .beginTransaction()
//                    .replace(R.id.content, fragment)
//                    .commit();
//            return true;
//        }
//        return false;
//    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;

        switch (menuItem.getItemId()) {
            case R.id.navigation_home:
                if (fragmentHome == null) {
                    fragmentHome = new Fragment_Home();
                    addFragment(fragmentHome);
                } else {
                    changeFragment(fragmentHome, selFragment, false);
                }
                selFragment = fragmentHome;
//                fragment = fragmentHome;
                break;
            case R.id.navigation_shortlist:
                if (fragmentShortlists == null) {
                    fragmentShortlists = new Fragment_Shortlists();
                    changeFragment(fragmentShortlists, selFragment, true);
                } else {
                    changeFragment(fragmentShortlists, selFragment, false);
                }
                selFragment = fragmentShortlists;
//                fragment = fragmentShortlists;
                break;
            case R.id.navigation_my_listings:
                if (fragmentMyListings == null) {
                    fragmentMyListings = new Fragment_MyListings();
                    changeFragment(fragmentMyListings, selFragment, true);
                } else {
                    changeFragment(fragmentMyListings, selFragment, false);
                }
                selFragment = fragmentMyListings;
//                fragment = fragmentMyListings;
                break;
            case R.id.navigation_profile:
                if (fragmentProfile == null) {
                    fragmentProfile = new Fragment_Profile();
                    changeFragment(fragmentProfile, selFragment, true);
                } else {
                    changeFragment(fragmentProfile, selFragment, false);
                }
                selFragment = fragmentProfile;
//                fragment = fragmentProfile;
                break;

            default:
                showToast(getString(R.string.message_under_development));
                break;
        }
//        return loadFragment(fragment);
        return true;
    }


    private void getInfo() {
        showProgressDialog();
        ApiInterface apiService = App.getClient_().create(ApiInterface.class);
        Call<Response_User_Info> call = apiService.getInfo(App.instance().getAccessToken());

        call.enqueue(new Callback<Response_User_Info>() {
            @Override
            public void onResponse(Call<Response_User_Info> call, Response<Response_User_Info> response) {
                dismissProgressDialog();
                int statusCode = response.code();
                if (response.body() != null) {
//                    Toast.makeText(Main_Act.this, String.valueOf(response.body().getUid()), Toast.LENGTH_SHORT).show();
                    App.instance().setUid(response.body().getUid());
                    App.instance().setUser(response.body());

                    long insertId = -1;
                    if (DBHelper.instance() != null) {
                        insertId = DBHelper.instance().insertUserDetail(response.body());
                    }
                }





            }

            @Override
            public void onFailure(Call<Response_User_Info> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, t.toString());
            }
        });
    }


    public int getCurrentMenu() {
        return mCurrentMenu;
    }

    private void addFragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(R.id.content, fragment);
        ft.show(fragment);
        ft.commit();
    }

    private void changeFragment(Fragment showFragment, Fragment hideFragment, boolean isTobeAdded) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.hide(hideFragment);
        if (isTobeAdded)
            ft.add(R.id.content, showFragment);
        ft.show(showFragment);
        ft.commitAllowingStateLoss();
        invalidateOptionsMenu();
    }


    private void commitFragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.content, fragment);
        ft.commit();
        invalidateOptionsMenu();
        toolbarProgressBar.setVisibility(View.GONE);
    }

    public void showToolbarProgress(boolean isShow) {
        if (isShow)
            toolbarProgressBar.setVisibility(View.VISIBLE);
        else
            toolbarProgressBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
//                go to location Activity
//                Intent in = new Intent(Main_Act.this, Location_Act.class);
//                startActivity(in);
                return (true);
        }
        return (super.onOptionsItemSelected(item));
    }


    private void logoutPopUp() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure want to logout?");
        alertDialogBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        doLogOut();
                    }
                });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void doLogOut() {
        Toast.makeText(this, "API Not Integrated, Please Try After Sometimes...", Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (!(selFragment instanceof Fragment_Home)) {
            onNavigationItemSelected(navigationView.getMenu().getItem(0));
            navigationView.setSelectedItemId(R.id.navigation_home);
        } else {
            if (doubleBackToExitPressedOnce) {
                finish();
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
//        locationTrack.stopListener();
    }
}
