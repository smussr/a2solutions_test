package com.netobjex.property_list.A_activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.netobjex.property_list.App;
import com.netobjex.property_list.BaseActivity;
import com.netobjex.property_list.R;
import com.netobjex.property_list.api.ApiInterface;
import com.netobjex.property_list.global.Constant;
import com.netobjex.property_list.global.PrefManager;
import com.netobjex.property_list.api.Response_Login;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login_Act extends BaseActivity {
    private static final String TAG = Login_Act.class.getSimpleName();
    private EditText edtEmail, edtPassword;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_);
    }

    @Override
    protected void initVariable() {
        edtEmail = findViewById(R.id.edt_email);
        edtPassword = findViewById(R.id.edt_password);
        btnLogin = findViewById(R.id.btn_login);
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void postInitView() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user_email = edtEmail.getText().toString().trim();
                String user_password = edtPassword.getText().toString().trim();
                String device_token = "testApp";
                String device_type = "android";
                String cart_id = "(Device ID)  ANDROID_ID";
                if (hasInternetConnection()) {
                    getAccessToken(user_email, user_password);
                } else showInternetMsg();
            }
        });
    }

    @Override
    protected void addAdapter() {

    }

    @Override
    protected void loadData() {

    }

    public void getAccessToken(String emailOrUsername, String password) {
        showProgressDialog();
        ApiInterface apiService = App.getClient_().create(ApiInterface.class);

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("emailOrUsername", emailOrUsername);
        params.put("password", password);

        Call<Response_Login> call = apiService.getCredentials(
                params);


        call.enqueue(new Callback<Response_Login>() {
            @Override
            public void onResponse(Call<Response_Login> call, Response<Response_Login> response) {
                dismissProgressDialog();
                int statusCode = response.code();
                if (response.body() != null) {
                    String accessToken = response.body().getAccessToken();
//                    Toast.makeText(Login_Act.this, accessToken, Toast.LENGTH_SHORT).show();

                    PrefManager.getPref(Login_Act.this).putString(Constant.PREF_KEY_SECURITY, accessToken);
                    App.instance().setAccessToken(accessToken);

                    Intent in = new Intent(Login_Act.this, Main_Act.class);
                    startActivity(in);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Response_Login> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, t.toString());
            }
        });
    }
}
