package com.netobjex.property_list.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static com.netobjex.property_list.db.DbConstants.DATABASE_NAME;
import static com.netobjex.property_list.db.DbConstants.DATABASE_VERSION;
import static com.netobjex.property_list.db.DbConstants.USER_TABLE;

public class SQLHelper extends SQLiteOpenHelper {

    public SQLHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DbConstants.CREATE_USER_TABLE);
        Log.e(">>>>>>>>>>", "Database Created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

        db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE);

        onCreate(db);

    }
}
