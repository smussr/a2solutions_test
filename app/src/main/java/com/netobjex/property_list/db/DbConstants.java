package com.netobjex.property_list.db;

/**
 * Created by Sreekanth on 24/07/19.
 */
public class DbConstants {
    public static final String DATABASE_NAME = "pl.db";
    public static final int DATABASE_VERSION = 1;
    public static final String USER_TABLE = "user";


    public static final String uid = "uid";
    public static final String email = "email";
    public static final String password = "password";
    public static final String firstName = "firstName";
    public static final String lastName = "lastName";
    public static final String fullName = "fullName";
    public static final String active = "active";
    public static final String username = "username";
    public static final String roles = "roles";
    public static final String image = "image";
    public static final String walletId = "walletId";
    public static final String country = "country";
    public static final String zipCode = "zipCode";
    public static final String phoneNumber = "phoneNumber";
    public static final String notification = "notification";


    /*
    *
  "uid": "35d5c49e-77bb-4bc1-9a68-0cb56c442f9e",
  "email": "mac@netobjex.com",
  "password": "$2a$10$IYI8lQceeiavaQHwe4k4NepHCJJOpBNyMyOCXbc863MYmVRUYxqUC",
  "firstName": null,
  "lastName": null,
  "fullName": "Lich Mac",
  "active": 1,
  "username": "mac@netobjex.com",
  "roles": "[\"sale\"]",
  "image": "https://netobjex-dev.s3.amazonaws.com/1554368488user_logo_35d5c49e-77bb-4bc1-9a68-0cb56c442f9e.png",
  "walletId": null,
  "country": "US",
  "zipCode": 90001,
  "phoneNumber": "84376307386",
  "notification": true
  */
    static final String CREATE_USER_TABLE = "CREATE TABLE " + USER_TABLE + "("
            + uid + " TEXT,"
            + email + " TEXT,"
            + password + " TEXT,"
            + firstName + " TEXT,"
            + lastName + " TEXT,"
            + fullName + " TEXT,"
            + active + " TEXT,"
            + username + " TEXT,"
            + roles + " TEXT,"
            + image + " TEXT,"
            + walletId + " TEXT,"
            + country + " TEXT,"
            + zipCode + " TEXT,"
            + phoneNumber + " TEXT,"
            + notification + " TEXT)";
}
