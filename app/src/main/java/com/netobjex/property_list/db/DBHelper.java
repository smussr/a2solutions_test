package com.netobjex.property_list.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.netobjex.property_list.App;
import com.netobjex.property_list.api.Response_User_Info;


public class DBHelper {

    private static final String TAG = "DBHelper";

    private static DBHelper dbHelper;
    private SQLiteDatabase db;

    public DBHelper(Context context) {
        db = new SQLHelper(context).getWritableDatabase();
    }

    public static synchronized DBHelper instance() {
        if (dbHelper == null) {
            dbHelper = new DBHelper(App.instance());
        }
        return dbHelper;
    }

    public long insertUserDetail(Response_User_Info userObj) {
        ContentValues values = new ContentValues();
        values.put(DbConstants.uid, userObj.getUid());
        values.put(DbConstants.email, userObj.getEmail());
        values.put(DbConstants.password, userObj.getPassword());
        values.put(DbConstants.firstName, String.valueOf(userObj.getFirstName()));
        values.put(DbConstants.lastName, String.valueOf(userObj.getLastName()));

        values.put(DbConstants.fullName, userObj.getFullName());
        values.put(DbConstants.active, userObj.getActive());
        values.put(DbConstants.username, userObj.getUsername());
        values.put(DbConstants.roles, userObj.getRoles());
        values.put(DbConstants.image, userObj.getImage());
        values.put(DbConstants.walletId, String.valueOf(userObj.getWalletId()));

        values.put(DbConstants.country, userObj.getCountry());
        values.put(DbConstants.zipCode, userObj.getZipCode());
        values.put(DbConstants.phoneNumber, userObj.getPhoneNumber());
        values.put(DbConstants.notification, String.valueOf(userObj.getNotification()));

        Log.i(TAG, "insertUserDetail------>" + values.toString());

        long val = db.insert(DbConstants.USER_TABLE, null, values);
        Log.i(TAG, "insertUserDetail STATUS ------>" + val);
        return val;
    }


    public Response_User_Info getUser() {
        Response_User_Info userDataObj = null;

        Cursor cur = db.query(DbConstants.USER_TABLE, null, null, null, null, null, null);

        if (cur != null && cur.moveToFirst()) {
            userDataObj = new Response_User_Info();
            userDataObj.setUid(cur.getString(cur.getColumnIndex(DbConstants.uid)));
            userDataObj.setEmail(cur.getString(cur.getColumnIndex(DbConstants.email)));
            userDataObj.setPassword(cur.getString(cur.getColumnIndex(DbConstants.password)));
            userDataObj.setFirstName(cur.getString(cur.getColumnIndex(DbConstants.firstName)));
            userDataObj.setLastName(String.valueOf(cur.getString(cur.getColumnIndex(DbConstants.lastName))));

            userDataObj.setFullName(String.valueOf(cur.getString(cur.getColumnIndex(DbConstants.fullName))));
            userDataObj.setActive(Integer.valueOf(String.valueOf(cur.getString(cur.getColumnIndex(DbConstants.active)))));
            userDataObj.setUsername(String.valueOf(cur.getString(cur.getColumnIndex(DbConstants.username))));
            userDataObj.setRoles(String.valueOf(cur.getString(cur.getColumnIndex(DbConstants.roles))));
            userDataObj.setImage(String.valueOf(cur.getString(cur.getColumnIndex(DbConstants.image))));
            userDataObj.setWalletId(String.valueOf(cur.getString(cur.getColumnIndex(DbConstants.walletId))));

            userDataObj.setCountry(String.valueOf(cur.getString(cur.getColumnIndex(DbConstants.country))));
            userDataObj.setZipCode(Integer.valueOf(String.valueOf(cur.getString(cur.getColumnIndex(DbConstants.zipCode)))));
            userDataObj.setPhoneNumber(String.valueOf(cur.getString(cur.getColumnIndex(DbConstants.phoneNumber))));
            userDataObj.setNotification(Boolean.valueOf(String.valueOf(cur.getString(cur.getColumnIndex(DbConstants.notification)))));
            /*
            *
            *
    public static final String uid = "uid";
    public static final String email = "email";
    public static final String password = "password";
    public static final String firstName = "firstName";
    public static final String lastName = "lastName";
    public static final String fullName = "fullName";
    public static final String active = "active";
    public static final String username = "username";
    public static final String roles = "roles";
    public static final String image = "image";
    public static final String walletId = "walletId";
    public static final String country = "country";
    public static final String zipCode = "zipCode";
    public static final String phoneNumber = "phoneNumber";
    public static final String notification = "notification";
*/

            Log.i(TAG, "getUser()======> " + cur.toString());
            cur.close();
        }

        return userDataObj;
    }

    public void logOutUser_(String uid) {
        db.delete(DbConstants.USER_TABLE, DbConstants.uid + " = ?", new String[]{uid});
    }
}