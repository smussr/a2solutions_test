package com.netobjex.property_list;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

//import com.netobjex.property_list.A_activity.MainActivity;
import com.netobjex.property_list.A_activity.Main_Act;
import com.netobjex.property_list.global.Constant;
import com.netobjex.property_list.global.Utility;

public abstract class BaseFragment extends Fragment implements Act_ImpMethods {

    private View view;
    private int res;
//    public MainActivity mActivity;
    public Main_Act mActivity;
    private ProgressDialog progressDialog;

    public void setContentView(int res) {
        this.res = res;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        Activity a;
//        if (context instanceof Activity){
//            a=(Activity) context;
//        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(res, container, false);

        setHasOptionsMenu(true);
        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        try {
            super.onActivityCreated(savedInstanceState);
            if (getActivity() instanceof Main_Act)
                mActivity = (Main_Act) getActivity();
            initializeView(getView());
            initVariable();
            postInitView();
            addAdapter();
            loadData();
        } catch (Exception e) {
            e.printStackTrace();
            showToast(e.toString());
            Utility.showLogE(BaseFragment.class, e.toString());
        }
    }

    protected boolean hasInternetConnection() {
        return Utility.isInternetConnected(getActivity());
    }


    protected final void showToast(String msg) {
        if (getActivity() != null)
            Toast.makeText(getActivity(), msg, Constant.TOAST_DURATION).show();
    }

    protected final void showInternetMsg() {

        if (getActivity() != null)
            showToast(getString(R.string.message_no_internet_found));
        //Snackbar.make(activity.get, "Please check your internet connection", Config.TOAST_DURATION).show();
    }

    protected final void showApiErrorMsg(String msg) {
        showToast(msg);
    }

    protected final void showServerErrorMsg() {
        showToast(getString(R.string.error_msg_api));
    }

    protected final void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(mActivity);
            progressDialog.setMessage("Please Wait...");
        }
        progressDialog.show();
    }

    protected final void showProgressDialog(boolean isCancellable) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(mActivity);
            progressDialog.setMessage("Please Wait...");
            progressDialog.setCanceledOnTouchOutside(isCancellable);
        }
        progressDialog.show();
    }

    protected void dismissProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

}
